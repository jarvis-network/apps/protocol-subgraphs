import { BigInt, store } from '@graphprotocol/graph-ts';
import {
  SelfMintingDerivativeDeployed,
  SelfMintingDerivativeRemoved,
} from '../../generated/SynthereumDeployer/SynthereumDeployer';

import { CreditLine_V2 } from '../../generated/templates';
import { getCreditLine, getOrInitDeployer, initCreditLine } from '../helpers/initializers';

export function handleNewCreditLine(event: SelfMintingDerivativeDeployed): void {
  const version = event.params.selfMintingDerivativeVersion;
  const creditLineAddress = event.params.selfMintingDerivative;
  if (version !== 2) return;
  let deployer = getOrInitDeployer(event.address);
  deployer.creditLineCount = deployer.creditLineCount.plus(BigInt.fromI32(1));
  deployer.save();

  initCreditLine(creditLineAddress, version, deployer);
  CreditLine_V2.create(creditLineAddress);
}

export function handleRemoveCreditLine(event: SelfMintingDerivativeRemoved): void {
  const creditLineAddress = event.params.selfMintingDerivative;
  let deployer = getOrInitDeployer(event.address);
  deployer.creditLineCount = deployer.creditLineCount.minus(BigInt.fromI32(1));
  deployer.save();
  let creditLine = getCreditLine(creditLineAddress);
  store.remove('CreditLine', creditLine.id);
}
