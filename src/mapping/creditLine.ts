import { BigInt, store } from '@graphprotocol/graph-ts';
import {
  PositionCreated,
  Deposit,
  Withdrawal,
  Repay,
  Redeem,
  Liquidation,
  EndedSponsorPosition,
} from '../../generated/templates/CreditLine_V2/CreditLine_V2';

import {
  getCreditLine,
  getCreditLineLPPosition,
  getOrInitCreditLineLPPosition,
  getOrInitUser,
} from '../helpers/initializers';
import {
  saveCreditLineDepositHistory,
  saveCreditLineMintHistory,
  saveCreditLineRedeemHistory,
  saveCreditLineRedeemHistoryByEndedSponsorPosition,
  saveCreditLineRepayHistory,
  saveCreditLineWithdrawHistory,
  saveCreditLineLiquidationHistory,
} from '../helpers/transaction';
import { updateCollateral, updateSynthetic, updateRatio } from '../logic/creditLine-logic';
import { getLPId } from '../utils/id-generation';
import { LPPosition } from '../../generated/schema';

export function handlePositionCreated(event: PositionCreated): void {
  let creditLine = getCreditLine(event.address);
  const lpAddress = event.params.sponsor;
  const collateralAmount = event.params.collateralAmount;
  const tokenAmount = event.params.tokenAmount;
  const feeAmount = event.params.feeAmount;
  let lpPosition = getOrInitCreditLineLPPosition(lpAddress, creditLine.id);

  updateCollateral(creditLine, lpPosition, collateralAmount, feeAmount, true);
  updateSynthetic(creditLine, lpPosition, tokenAmount, true);
  updateRatio(creditLine, lpPosition);
  saveCreditLineMintHistory(event, lpPosition.user, creditLine);
}

export function handleDeposit(event: Deposit): void {
  let creditLine = getCreditLine(event.address);
  const lpAddress = event.params.sponsor;
  const amountDeposited = event.params.collateralAmount;
  let lpPosition = getCreditLineLPPosition(lpAddress, creditLine.id);
  updateCollateral(creditLine, lpPosition, amountDeposited, BigInt.fromI32(0), true);
  updateRatio(creditLine, lpPosition);
  saveCreditLineDepositHistory(event, getOrInitUser(lpAddress).id, creditLine);
}

export function handleWithdrawal(event: Withdrawal): void {
  let creditLine = getCreditLine(event.address);
  const lpAddress = event.params.sponsor;
  const collateralAmountWithdrawn = event.params.collateralAmount;
  let lpPosition = getCreditLineLPPosition(lpAddress, creditLine.id);
  updateCollateral(creditLine, lpPosition, collateralAmountWithdrawn, BigInt.fromI32(0), false);
  updateRatio(creditLine, lpPosition);
  saveCreditLineWithdrawHistory(event, getOrInitUser(lpAddress).id, creditLine);
}
export function handleRepay(event: Repay): void {
  let creditLine = getCreditLine(event.address);
  const lpAddress = event.params.sponsor;
  const tokenRepaid = event.params.numTokensRepaid;
  let lpPosition = getCreditLineLPPosition(lpAddress, creditLine.id);
  updateSynthetic(creditLine, lpPosition, tokenRepaid, false);
  updateRatio(creditLine, lpPosition);
  saveCreditLineRepayHistory(event, getOrInitUser(lpAddress).id, creditLine);
}
export function handleRedeem(event: Redeem): void {
  let creditLine = getCreditLine(event.address);
  const lpAddress = event.params.sponsor;
  const tokenRepaid = event.params.tokenAmount;
  const collateralAmountWithdrawn = event.params.collateralAmount;

  // Handle case where EndedSponsorPosition can be trigger before Redeem event
  let lpId = getLPId(lpAddress, creditLine.id);
  let lpPosition = LPPosition.load(lpId);
  if (lpPosition) {
    updateSynthetic(creditLine, lpPosition, tokenRepaid, false);
    updateCollateral(creditLine, lpPosition, collateralAmountWithdrawn, BigInt.fromI32(0), false);
    updateRatio(creditLine, lpPosition);
    saveCreditLineRedeemHistory(event, getOrInitUser(lpAddress).id, creditLine);
  }
}

export function handleEndedSponsorPosition(event: EndedSponsorPosition): void {
  let creditLine = getCreditLine(event.address);
  const lpAddress = event.params.sponsor;
  let lpPosition = getCreditLineLPPosition(lpAddress, creditLine.id);
  const lpTokenRepaid = lpPosition.syntheticOutstanding;
  const lpCollateralWithdrawn = lpPosition.netCollateral;
  updateSynthetic(creditLine, lpPosition, lpTokenRepaid, false);
  updateCollateral(creditLine, lpPosition, lpCollateralWithdrawn, BigInt.fromI32(0), false);
  updateRatio(creditLine, lpPosition);

  saveCreditLineRedeemHistoryByEndedSponsorPosition(
    event,
    lpTokenRepaid,
    lpCollateralWithdrawn,
    getOrInitUser(lpAddress).id,
    creditLine
  );
  store.remove('LPPosition', lpPosition.id);
}

export function handleLiquidation(event: Liquidation): void {
  let creditLine = getCreditLine(event.address);
  const lpAddress = event.params.sponsor;
  let lpPosition = getCreditLineLPPosition(lpAddress, creditLine.id);
  const liquidatedTokens = event.params.liquidatedTokens;
  const liquidatedCollateral = event.params.liquidatedCollateral;

  updateSynthetic(creditLine, lpPosition, liquidatedTokens, false);
  updateCollateral(creditLine, lpPosition, liquidatedCollateral, BigInt.fromI32(0), false);
  updateRatio(creditLine, lpPosition);

  const isEndedPosition = lpPosition.syntheticOutstanding.isZero();
  saveCreditLineLiquidationHistory(event, getOrInitUser(lpAddress).id, creditLine, isEndedPosition);
  if (isEndedPosition) {
    store.remove('LPPosition', lpPosition.id);
  }
}
