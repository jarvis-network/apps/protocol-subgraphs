import { Address, ethereum } from '@graphprotocol/graph-ts';

export function getHistoryEntityId(event: ethereum.Event): string {
  return (
    event.block.number.toString() +
    ':' +
    event.transaction.index.toString() +
    ':' +
    event.transaction.hash.toHexString() +
    ':' +
    event.logIndex.toString() +
    ':' +
    event.transactionLogIndex.toString()
  );
}

export function getDeployerId(deployerAddress: Address): string {
  return deployerAddress.toHexString();
}

export function getPoolId(poolAddress: Address): string {
  return poolAddress.toHexString();
}

export function getCreditLineId(creditLineAddress: Address): string {
  return creditLineAddress.toHexString();
}

export function getTokenId(tokenAddress: Address): string {
  return tokenAddress.toHexString();
}

export function getUserId(userAddress: Address): string {
  return userAddress.toHexString();
}

export function getLPId(lpAddress: Address, poolOrCreditLineId: string): string {
  return poolOrCreditLineId + '-' + lpAddress.toHexString();
}
