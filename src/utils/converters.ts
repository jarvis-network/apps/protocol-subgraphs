import {
  BigInt,
  BigDecimal,
  Bytes,
  ByteArray,
  crypto,
  log,
  Address,
} from '@graphprotocol/graph-ts';
import { ADDRESS_ZERO } from './constants';

export function zeroBD(): BigDecimal {
  return BigDecimal.fromString('0');
}

export function zeroBI(): BigInt {
  return BigInt.fromI32(0);
}

export function zeroAddress(): Bytes {
  return Bytes.fromHexString(ADDRESS_ZERO);
}

// @ts-ignore
export function exponentToBigDecimal(decimals: i32): BigDecimal {
  let bd = BigDecimal.fromString('1');
  let bd10 = BigDecimal.fromString('10');
  for (let i = 0; i < decimals; i++) {
    bd = bd.times(bd10);
  }
  return bd;
}

// @ts-ignore
export function exponentToBigInt(decimals: i32): BigInt {
  let bi = BigInt.fromI32(1);
  let bi10 = BigInt.fromI32(10);
  for (let i = 0; i < decimals; i++) {
    bi = bi.times(bi10);
  }
  return bi;
}
// @ts-ignore
export function convertAmountToDecimals(amount: BigInt, decimals: i32): BigDecimal {
  return amount.toBigDecimal().div(exponentToBigDecimal(decimals));
}

export function convertTokenDe(amount: BigInt): BigInt {
  return amount.times(exponentToBigInt(12));
}

// @ts-ignore
export function formatAmountTo18Decimals(amount: BigInt, baseDecimals: i32): BigInt {
  if (amount.isZero()) return zeroBI();
  if (baseDecimals == 18) return amount;
  if (baseDecimals > 18) {
    return amount.div(exponentToBigInt(baseDecimals - 18));
  }
  return amount.times(exponentToBigInt(18 - baseDecimals));
}
