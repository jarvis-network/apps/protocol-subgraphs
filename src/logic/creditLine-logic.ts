import { BigDecimal, BigInt, log } from '@graphprotocol/graph-ts';
import { CreditLine, LPPosition, Token } from '../../generated/schema';
import { calcRatio } from '../helpers/math';
import { formatAmountTo18Decimals, zeroBD } from '../utils/converters';

export function updateCollateral(
  creditLine: CreditLine,
  lpPosition: LPPosition,
  amount: BigInt,
  feeAmount: BigInt,
  isDeposit: boolean
): void {
  if (isDeposit) {
    creditLine.netCollateral = creditLine.netCollateral.plus(amount.minus(feeAmount));
    lpPosition.netCollateral = lpPosition.netCollateral.plus(amount.minus(feeAmount));
  } else {
    creditLine.netCollateral = creditLine.netCollateral.minus(amount);
    lpPosition.netCollateral = lpPosition.netCollateral.minus(amount);
  }
  if (feeAmount.gt(BigInt.fromI32(0))) {
    addFees(creditLine, lpPosition, feeAmount);
  }
  creditLine.save();
  lpPosition.save();
}

export function updateSynthetic(
  creditLine: CreditLine,
  lpPosition: LPPosition,
  amount: BigInt,
  isMint: boolean
): void {
  if (isMint) {
    creditLine.syntheticOutstanding = creditLine.syntheticOutstanding.plus(amount);
    lpPosition.syntheticOutstanding = lpPosition.syntheticOutstanding.plus(amount);
  } else {
    creditLine.syntheticOutstanding = creditLine.syntheticOutstanding.minus(amount);
    lpPosition.syntheticOutstanding = lpPosition.syntheticOutstanding.minus(amount);
  }
  creditLine.save();
  lpPosition.save();
}

export function addFees(creditLine: CreditLine, lpPosition: LPPosition, amount: BigInt): void {
  creditLine.feeAmount = creditLine.feeAmount.plus(amount);
  lpPosition.feeAmount = lpPosition.feeAmount.plus(amount);
  creditLine.save();
  lpPosition.save();
}

// having 0 of outstanding synthetic and > 0 of collateral deposited
// it's not possible from contract logic
export function updateRatio(creditLine: CreditLine, lpPosition: LPPosition): void {
  const collateralToken = Token.load(creditLine.collateralToken);
  if (!collateralToken) {
    log.error('token {} does not exist', [creditLine.collateralToken]);
    throw new Error('token ' + creditLine.collateralToken + ' does not exist');
  }
  if (creditLine.syntheticOutstanding.isZero()) {
    creditLine.ratio = zeroBD();
  } else {
    const creditLineCollateralAmountFormated = formatAmountTo18Decimals(
      creditLine.netCollateral,
      collateralToken.decimals
    );
    creditLine.ratio = calcRatio(
      creditLineCollateralAmountFormated,
      creditLine.syntheticOutstanding
    );
  }
  if (lpPosition.syntheticOutstanding.isZero()) {
    lpPosition.ratio = zeroBD();
  } else {
    const LpCollateralAmountFormated = formatAmountTo18Decimals(
      lpPosition.netCollateral,
      collateralToken.decimals
    );
    lpPosition.ratio = calcRatio(LpCollateralAmountFormated, lpPosition.syntheticOutstanding);
  }
  creditLine.save();
  lpPosition.save();
}
