import { Address, log } from '@graphprotocol/graph-ts';
import { CreditLine, Deployer, LPPosition, Token, User } from '../../generated/schema';
import { IERC20Detailed } from '../../generated/templates/CreditLine_V2/IERC20Detailed';
import { CreditLine_V2 } from '../../generated/templates/CreditLine_V2/CreditLine_V2';
import {
  getDeployerId,
  getLPId,
  getTokenId,
  getUserId,
  getCreditLineId,
} from '../utils/id-generation';
import { convertAmountToDecimals, zeroBD, zeroBI } from '../utils/converters';

export function getOrInitDeployer(deployerAddress: Address): Deployer {
  let deployerId = getDeployerId(deployerAddress);
  let deployer = Deployer.load(deployerId);
  if (!deployer) {
    deployer = new Deployer(deployerId);
    deployer.creditLineCount = zeroBI();
    deployer.save();
  }
  return deployer;
}

export function getCreditLine(creditLineAddress: Address): CreditLine {
  const creditLineId = getCreditLineId(creditLineAddress);
  let creditLine = CreditLine.load(creditLineId);
  if (!creditLine) {
    log.error('contract {} does not exist as creditLine entity', [creditLineId]);
    throw new Error('creditLine ' + creditLineId + ' does not exist');
  }
  return creditLine;
}

export function initCreditLine(
  creditLineAddress: Address,
  creditLineVersion: i32,
  deployer: Deployer
): void {
  const creditLineId = getCreditLineId(creditLineAddress);
  let creditLine = CreditLine.load(creditLineId);
  if (creditLine) {
    log.error('creditLineId {} has been already initialized', [creditLineId]);
    throw new Error('creditLine ' + creditLineId + ' already exist');
  }

  creditLine = new CreditLine(creditLineId);
  creditLine.version = creditLineVersion;
  creditLine.netCollateral = zeroBI();
  creditLine.syntheticOutstanding = zeroBI();
  creditLine.feeAmount = zeroBI();
  creditLine.ratio = zeroBD();

  let creditLineContract = CreditLine_V2.bind(creditLineAddress);
  let collateralTokenAddress = creditLineContract.collateralToken();
  let collateralToken = getOrInitToken(collateralTokenAddress);
  let syntheticTokenAddress = creditLineContract.syntheticToken();
  let syntheticToken = getOrInitToken(syntheticTokenAddress);
  let collateralRequirement = creditLineContract.collateralRequirement();
  creditLine.priceIdentifier = creditLineContract.priceIdentifier();

  creditLine.collateralRequirement = convertAmountToDecimals(collateralRequirement, 18);
  creditLine.collateralToken = collateralToken.id;
  creditLine.syntheticToken = syntheticToken.id;
  creditLine.deployer = deployer.id;
  creditLine.save();
}

export function getOrInitToken(tokenCurrencyAddress: Address): Token {
  let tokenId = getTokenId(tokenCurrencyAddress);
  let token = Token.load(tokenId);
  if (!token) {
    token = new Token(tokenId);
    let ERC20Contract = IERC20Detailed.bind(tokenCurrencyAddress);
    token.symbol = ERC20Contract.symbol();
    token.decimals = ERC20Contract.decimals();
    token.save();
  }
  return token;
}

export function getOrInitCreditLineLPPosition(
  lpAddress: Address,
  creditLineId: string
): LPPosition {
  let lpId = getLPId(lpAddress, creditLineId);
  let creditLinePosition = LPPosition.load(lpId);
  if (creditLinePosition) {
    return creditLinePosition;
  }
  creditLinePosition = new LPPosition(lpId);
  creditLinePosition.creditLine = creditLineId;
  creditLinePosition.netCollateral = zeroBI();
  creditLinePosition.syntheticOutstanding = zeroBI();
  creditLinePosition.feeAmount = zeroBI();
  creditLinePosition.netCollateral = zeroBI();
  creditLinePosition.ratio = zeroBD();
  creditLinePosition.user = lpAddress.toHexString();

  creditLinePosition.save();

  let user = getOrInitUser(lpAddress);
  const userPositions = user.positions;
  userPositions.push(lpId);
  user.positions = userPositions;
  user.save();

  return creditLinePosition;
}

export function getCreditLineLPPosition(lpAddress: Address, creditLineId: string): LPPosition {
  let lpId = getLPId(lpAddress, creditLineId);
  let lpPosition = LPPosition.load(lpId);
  if (!lpPosition) {
    log.error('lpPosition {} does not exist for this pool', [lpId]);
    throw new Error('LP position ' + lpId + ' does not exist');
  }
  return lpPosition;
}

export function getOrInitUser(userAddress: Address): User {
  let userId = getUserId(userAddress);
  let user = User.load(userId);
  if (!user) {
    user = new User(userId);
    user.positions = [];
    user.save();
  }
  return user;
}
