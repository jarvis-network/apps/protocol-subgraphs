import { BigDecimal, BigInt } from '@graphprotocol/graph-ts';

export function calcRatio(collateral: BigInt, token: BigInt): BigDecimal {
  return collateral.toBigDecimal().div(token.toBigDecimal());
}
