import {
  CreditLine,
  Mint as MintAction,
  Deposit as DepositAction,
  Withdraw as WithdrawAction,
  Repay as RepayAction,
  Redeem as RedeemAction,
  Liquidate as LiquidateAction,
} from '../../generated/schema';
import { getHistoryEntityId } from '../utils/id-generation';
import {
  PositionCreated,
  Deposit,
  Withdrawal,
  Repay,
  Redeem,
  Liquidation,
} from '../../generated/templates/CreditLine_V2/CreditLine_V2';

import { getOrInitUser } from './initializers';
import { BigInt, ethereum } from '@graphprotocol/graph-ts';

export function saveCreditLineMintHistory(
  event: PositionCreated,
  sponsorId: string,
  creditLine: CreditLine
): void {
  let mint = new MintAction(getHistoryEntityId(event));

  mint.txHash = event.transaction.hash;
  mint.blockNumber = event.block.number;
  mint.timestamp = event.block.timestamp.toI32();
  mint.action = 'Mint';
  mint.user = sponsorId;
  mint.creditLine = creditLine.id;

  mint.syntheticMinted = event.params.tokenAmount;
  mint.syntheticToken = creditLine.syntheticToken;
  mint.collateralDeposited = event.params.collateralAmount;
  mint.feeAmount = event.params.feeAmount;
  mint.collateralToken = creditLine.collateralToken;
  mint.save();
}

export function saveCreditLineDepositHistory(
  event: Deposit,
  sponsorId: string,
  creditLine: CreditLine
): void {
  let deposit = new DepositAction(getHistoryEntityId(event));
  deposit.txHash = event.transaction.hash;
  deposit.blockNumber = event.block.number;
  deposit.timestamp = event.block.timestamp.toI32();
  deposit.action = 'Deposit';
  deposit.user = sponsorId;
  deposit.caller = getOrInitUser(event.transaction.from).id;
  deposit.creditLine = creditLine.id;

  deposit.collateralAmount = event.params.collateralAmount;
  deposit.collateralToken = creditLine.collateralToken;
  deposit.save();
}

export function saveCreditLineWithdrawHistory(
  event: Withdrawal,
  sponsorId: string,
  creditLine: CreditLine
): void {
  let withdraw = new WithdrawAction(getHistoryEntityId(event));
  withdraw.txHash = event.transaction.hash;
  withdraw.blockNumber = event.block.number;
  withdraw.timestamp = event.block.timestamp.toI32();
  withdraw.action = 'Withdraw';
  withdraw.user = sponsorId;
  withdraw.creditLine = creditLine.id;

  withdraw.collateralAmount = event.params.collateralAmount;
  withdraw.collateralToken = creditLine.collateralToken;
  withdraw.save();
}
export function saveCreditLineRepayHistory(
  event: Repay,
  sponsorId: string,
  creditLine: CreditLine
): void {
  let repay = new RepayAction(getHistoryEntityId(event));
  repay.txHash = event.transaction.hash;
  repay.blockNumber = event.block.number;
  repay.timestamp = event.block.timestamp.toI32();
  repay.action = 'Repay';
  repay.user = sponsorId;
  repay.creditLine = creditLine.id;

  repay.tokenAmount = event.params.numTokensRepaid;
  repay.syntheticToken = creditLine.syntheticToken;
  repay.save();
}

export function saveCreditLineRedeemHistory(
  event: Redeem,
  sponsorId: string,
  creditLine: CreditLine
): void {
  let redeem = new RedeemAction(getHistoryEntityId(event));
  redeem.txHash = event.transaction.hash;
  redeem.blockNumber = event.block.number;
  redeem.timestamp = event.block.timestamp.toI32();
  redeem.action = 'Redeem';
  redeem.user = sponsorId;
  redeem.creditLine = creditLine.id;
  redeem.isEndedPosition = false;

  redeem.tokenAmount = event.params.tokenAmount;
  redeem.syntheticToken = creditLine.syntheticToken;
  redeem.collateralAmount = event.params.collateralAmount;
  redeem.collateralToken = creditLine.collateralToken;
  redeem.save();
}

export function saveCreditLineEndedPositionHistory(
  event: Redeem,
  sponsorId: string,
  creditLine: CreditLine
): void {
  let redeem = new RedeemAction(getHistoryEntityId(event));
  redeem.txHash = event.transaction.hash;
  redeem.blockNumber = event.block.number;
  redeem.timestamp = event.block.timestamp.toI32();
  redeem.action = 'Redeem';
  redeem.user = sponsorId;
  redeem.creditLine = creditLine.id;

  redeem.tokenAmount = event.params.tokenAmount;
  redeem.syntheticToken = creditLine.syntheticToken;
  redeem.collateralAmount = event.params.collateralAmount;
  redeem.collateralToken = creditLine.collateralToken;
  redeem.save();
}

export function saveCreditLineRedeemHistoryByEndedSponsorPosition(
  event: ethereum.Event,
  tokenAmountRepaid: BigInt,
  collateralAmountWithdrawn: BigInt,
  sponsorId: string,
  creditLine: CreditLine
): void {
  let redeem = new RedeemAction(getHistoryEntityId(event));
  redeem.txHash = event.transaction.hash;
  redeem.blockNumber = event.block.number;
  redeem.timestamp = event.block.timestamp.toI32();
  redeem.action = 'Redeem';
  redeem.user = sponsorId;
  redeem.creditLine = creditLine.id;
  redeem.isEndedPosition = true;

  redeem.tokenAmount = tokenAmountRepaid;
  redeem.syntheticToken = creditLine.syntheticToken;
  redeem.collateralAmount = collateralAmountWithdrawn;
  redeem.collateralToken = creditLine.collateralToken;
  redeem.save();
}

export function saveCreditLineLiquidationHistory(
  event: Liquidation,
  sponsorId: string,
  creditLine: CreditLine,
  isEndedPosition: boolean
): void {
  let liquidate = new LiquidateAction(getHistoryEntityId(event));
  liquidate.txHash = event.transaction.hash;
  liquidate.blockNumber = event.block.number;
  liquidate.timestamp = event.block.timestamp.toI32();
  liquidate.action = 'Liquidate';
  liquidate.user = sponsorId;
  liquidate.liquidator = getOrInitUser(event.params.liquidator).id;

  liquidate.creditLine = creditLine.id;
  liquidate.isEndedPosition = isEndedPosition;

  liquidate.tokenAmount = event.params.liquidatedTokens;
  liquidate.syntheticToken = creditLine.syntheticToken;
  liquidate.collateralAmount = event.params.liquidatedCollateral;
  liquidate.collateralRewardsAmount = event.params.collateralReward;
  liquidate.collateralToken = creditLine.collateralToken;
  liquidate.save();
}
