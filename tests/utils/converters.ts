import { Bytes } from '@graphprotocol/graph-ts';

export function stringToBytes32(str: string): Bytes {
  let bytes = Bytes.fromUTF8(str);
  let bytes32 = new Uint8Array(32);

  for (let i = 0; i < bytes.length && i < 32; i++) {
    bytes32[i] = bytes[i];
  }

  return Bytes.fromUint8Array(bytes32);
}
