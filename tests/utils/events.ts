import { Address, BigInt, ethereum } from '@graphprotocol/graph-ts';
import { createMockedFunction, newMockEvent } from 'matchstick-as/assembly/index';
import {
  SelfMintingDerivativeDeployed,
  SelfMintingDerivativeRemoved,
} from '../../generated/SynthereumDeployer/SynthereumDeployer';
import { handleNewCreditLine } from '../../src/mapping/deployer';
import { CreditLineConfig, DEPLOYER_ADDRESS, TokenConfig } from './constants';
import {
  PositionCreated,
  Deposit,
  Withdrawal,
  Repay,
  Redeem,
  Liquidation,
  EndedSponsorPosition,
} from '../../generated/templates/CreditLine_V2/CreditLine_V2';
import { stringToBytes32 } from './converters';
import { handleDeposit, handlePositionCreated } from '../../src/mapping/creditLine';

export function handleNewCreditLines(events: SelfMintingDerivativeDeployed[]): void {
  events.forEach((event) => {
    handleNewCreditLine(event);
  });
}

export function createNewCreditLineEvent(
  creditLine: CreditLineConfig,
  collateral: TokenConfig,
  synthetic: TokenConfig
): SelfMintingDerivativeDeployed {
  const creditLineAddress = Address.fromString(creditLine.address);
  const collateralTokenAddress = Address.fromString(collateral.address);
  const syntheticTokenAddress = Address.fromString(synthetic.address);

  createMockedFunction(
    creditLineAddress,
    'collateralRequirement',
    'collateralRequirement():(uint256)'
  ).returns([ethereum.Value.fromUnsignedBigInt(creditLine.collateralRequirement)]);
  createMockedFunction(creditLineAddress, 'priceIdentifier', 'priceIdentifier():(bytes32)').returns(
    [ethereum.Value.fromBytes(stringToBytes32(creditLine.priceIdentifier))]
  );
  createMockedFunction(creditLineAddress, 'collateralToken', 'collateralToken():(address)').returns(
    [ethereum.Value.fromAddress(collateralTokenAddress)]
  );
  createMockedFunction(collateralTokenAddress, 'symbol', 'symbol():(string)').returns([
    ethereum.Value.fromString(collateral.symbol),
  ]);
  createMockedFunction(collateralTokenAddress, 'decimals', 'decimals():(uint8)').returns([
    ethereum.Value.fromI32(collateral.decimals),
  ]);
  createMockedFunction(creditLineAddress, 'syntheticToken', 'syntheticToken():(address)').returns([
    ethereum.Value.fromAddress(syntheticTokenAddress),
  ]);
  createMockedFunction(syntheticTokenAddress, 'symbol', 'symbol():(string)').returns([
    ethereum.Value.fromString(synthetic.symbol),
  ]);
  createMockedFunction(syntheticTokenAddress, 'decimals', 'decimals():(uint8)').returns([
    ethereum.Value.fromI32(synthetic.decimals),
  ]);
  // @ts-ignore
  let newCreditLineEvent = changetype<SelfMintingDerivativeDeployed>(newMockEvent());
  newCreditLineEvent.parameters = new Array();
  let creditLineVersionParam = new ethereum.EventParam(
    'selfMintingDerivativeVersion',
    ethereum.Value.fromI32(creditLine.version)
  );
  let creditLineAddressParam = new ethereum.EventParam(
    'selfMintingDerivative',
    ethereum.Value.fromAddress(creditLineAddress)
  );

  newCreditLineEvent.parameters.push(creditLineVersionParam);
  newCreditLineEvent.parameters.push(creditLineAddressParam);

  newCreditLineEvent.address = Address.fromString(DEPLOYER_ADDRESS);
  return newCreditLineEvent;
}
export function createRemoveCreditLineEvent(creditLine: string): SelfMintingDerivativeRemoved {
  const creditLineAddress = Address.fromString(creditLine);

  // @ts-ignore
  let removeCreditLineEvent = changetype<SelfMintingDerivativeRemoved>(newMockEvent());
  removeCreditLineEvent.parameters = new Array();
  let creditLineAddressParam = new ethereum.EventParam(
    'selfMintingDerivative',
    ethereum.Value.fromAddress(creditLineAddress)
  );
  removeCreditLineEvent.parameters.push(creditLineAddressParam);

  removeCreditLineEvent.address = Address.fromString(DEPLOYER_ADDRESS);
  return removeCreditLineEvent;
}

export function handlePositionsCreated(events: PositionCreated[]): void {
  events.forEach((event) => {
    handlePositionCreated(event);
  });
}

export function createCreditLinePositionEvent(
  creditLine: string,
  sponsor: string,
  collateralAmount: BigInt,
  tokenAmount: BigInt,
  feeAmount: BigInt
): PositionCreated {
  // @ts-ignore
  let newPositionEvent = changetype<PositionCreated>(newMockEvent());
  newPositionEvent.parameters = new Array();
  let sponsorParam = new ethereum.EventParam(
    'sponsor',
    ethereum.Value.fromAddress(Address.fromString(sponsor))
  );
  let collateralAmountParam = new ethereum.EventParam(
    'collateralAmount',
    ethereum.Value.fromUnsignedBigInt(collateralAmount)
  );
  let tokenAmountParam = new ethereum.EventParam(
    'tokenAmount',
    ethereum.Value.fromUnsignedBigInt(tokenAmount)
  );
  let feeAmountParam = new ethereum.EventParam(
    'feeAmount',
    ethereum.Value.fromUnsignedBigInt(feeAmount)
  );
  newPositionEvent.parameters.push(sponsorParam);
  newPositionEvent.parameters.push(collateralAmountParam);
  newPositionEvent.parameters.push(tokenAmountParam);
  newPositionEvent.parameters.push(feeAmountParam);

  newPositionEvent.address = Address.fromString(creditLine);
  return newPositionEvent;
}

export function createCreditLineEndedSponsorPositionEvent(
  creditLine: string,
  sponsor: string
): EndedSponsorPosition {
  // @ts-ignore
  let endPositionEvent = changetype<EndedSponsorPosition>(newMockEvent());
  endPositionEvent.parameters = new Array();
  let sponsorParam = new ethereum.EventParam(
    'sponsor',
    ethereum.Value.fromAddress(Address.fromString(sponsor))
  );

  endPositionEvent.parameters.push(sponsorParam);

  endPositionEvent.address = Address.fromString(creditLine);
  return endPositionEvent;
}

export function createCreditLineDepositEvent(
  creditLine: string,
  sponsor: string,
  collateralAmount: BigInt
): Deposit {
  // @ts-ignore
  let newDepositEvent = changetype<Deposit>(newMockEvent());
  newDepositEvent.parameters = new Array();
  let sponsorParam = new ethereum.EventParam(
    'sponsor',
    ethereum.Value.fromAddress(Address.fromString(sponsor))
  );
  let collateralAmountParam = new ethereum.EventParam(
    'collateralAmount',
    ethereum.Value.fromUnsignedBigInt(collateralAmount)
  );

  newDepositEvent.parameters.push(sponsorParam);
  newDepositEvent.parameters.push(collateralAmountParam);

  newDepositEvent.address = Address.fromString(creditLine);
  return newDepositEvent;
}

export function createCreditLineWithdrawalEvent(
  creditLine: string,
  sponsor: string,
  collateralAmount: BigInt
): Withdrawal {
  // @ts-ignore
  let newWithdrawEvent = changetype<Withdrawal>(newMockEvent());
  newWithdrawEvent.parameters = new Array();
  let sponsorParam = new ethereum.EventParam(
    'sponsor',
    ethereum.Value.fromAddress(Address.fromString(sponsor))
  );
  let collateralAmountParam = new ethereum.EventParam(
    'collateralAmount',
    ethereum.Value.fromUnsignedBigInt(collateralAmount)
  );

  newWithdrawEvent.parameters.push(sponsorParam);
  newWithdrawEvent.parameters.push(collateralAmountParam);

  newWithdrawEvent.address = Address.fromString(creditLine);
  return newWithdrawEvent;
}

export function createCreditLineRedeemEvent(
  creditLine: string,
  sponsor: string,
  collateralAmount: BigInt,
  tokenAmount: BigInt
): Redeem {
  // @ts-ignore
  let newRedeemEvent = changetype<Redeem>(newMockEvent());
  newRedeemEvent.parameters = new Array();
  let sponsorParam = new ethereum.EventParam(
    'sponsor',
    ethereum.Value.fromAddress(Address.fromString(sponsor))
  );
  let collateralAmountParam = new ethereum.EventParam(
    'collateralAmount',
    ethereum.Value.fromUnsignedBigInt(collateralAmount)
  );
  let tokenAmountParam = new ethereum.EventParam(
    'tokenAmount',
    ethereum.Value.fromUnsignedBigInt(tokenAmount)
  );

  newRedeemEvent.parameters.push(sponsorParam);
  newRedeemEvent.parameters.push(collateralAmountParam);
  newRedeemEvent.parameters.push(tokenAmountParam);

  newRedeemEvent.address = Address.fromString(creditLine);
  return newRedeemEvent;
}

export function createCreditLineRepayEvent(
  creditLine: string,
  sponsor: string,
  numTokensRepaid: BigInt,
  newTokenCount: BigInt
): Repay {
  // @ts-ignore
  let newRepayEvent = changetype<Repay>(newMockEvent());
  newRepayEvent.parameters = new Array();
  let sponsorParam = new ethereum.EventParam(
    'sponsor',
    ethereum.Value.fromAddress(Address.fromString(sponsor))
  );
  let numTokensRepaidParam = new ethereum.EventParam(
    'numTokensRepaid',
    ethereum.Value.fromUnsignedBigInt(numTokensRepaid)
  );
  let newTokenCountParam = new ethereum.EventParam(
    'newTokenCount',
    ethereum.Value.fromUnsignedBigInt(newTokenCount)
  );

  newRepayEvent.parameters.push(sponsorParam);
  newRepayEvent.parameters.push(numTokensRepaidParam);
  newRepayEvent.parameters.push(newTokenCountParam);

  newRepayEvent.address = Address.fromString(creditLine);
  return newRepayEvent;
}

export function createCreditLineLiquidateEvent(
  creditLine: string,
  sponsor: string,
  liquidator: string,
  amountLiquidatedTokens: BigInt,
  amountLiquidatedCollateral: BigInt,
  amountCollateralReward: BigInt,
  liquidationTime: BigInt
): Liquidation {
  // @ts-ignore
  let newLiquidationEvent = changetype<Liquidation>(newMockEvent());
  newLiquidationEvent.parameters = new Array();

  let sponsorParam = new ethereum.EventParam(
    'sponsor',
    ethereum.Value.fromAddress(Address.fromString(sponsor))
  );
  let liquidatorParam = new ethereum.EventParam(
    'liquidator',
    ethereum.Value.fromAddress(Address.fromString(liquidator))
  );
  let amountLiquidatedTokensParam = new ethereum.EventParam(
    'liquidatedTokens',
    ethereum.Value.fromUnsignedBigInt(amountLiquidatedTokens)
  );
  let amountLiquidatedCollateralParam = new ethereum.EventParam(
    'liquidatedCollateral',
    ethereum.Value.fromUnsignedBigInt(amountLiquidatedCollateral)
  );
  let amountCollateralRewardParam = new ethereum.EventParam(
    'liquidatedCollateral',
    ethereum.Value.fromUnsignedBigInt(amountCollateralReward)
  );
  let liquidationTimeParam = new ethereum.EventParam(
    'liquidationTime',
    ethereum.Value.fromUnsignedBigInt(liquidationTime)
  );

  newLiquidationEvent.parameters.push(sponsorParam);
  newLiquidationEvent.parameters.push(liquidatorParam);
  newLiquidationEvent.parameters.push(amountLiquidatedTokensParam);
  newLiquidationEvent.parameters.push(amountLiquidatedCollateralParam);
  newLiquidationEvent.parameters.push(amountCollateralRewardParam);
  newLiquidationEvent.parameters.push(liquidationTimeParam);

  newLiquidationEvent.address = Address.fromString(creditLine);
  return newLiquidationEvent;
}
