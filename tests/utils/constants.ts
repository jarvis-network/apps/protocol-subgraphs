import { BigInt } from '@graphprotocol/graph-ts';

export const DEPLOYER_ADDRESS = '0x2CfdBeA1d21A30d627e87D946c5E3e0587Cf7Ec9';
export const jEUR_USDC_CREDITLINE_V2_ADDRESS = '0x4154550f4Db74Dc38d1FE98e1F3F28ed6daD627d';
export const jBRL_USDC_CREDITLINE_V2_ADDRESS = '0x38f3066a3C487556f5be53919f7E75B6ef41E439';
export const USDC_ADDRESS = '0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174';
export const jEUR_ADDRESS = '0x4e3Decbb3645551B8A19f0eA1678079FCB33fB4c';
export const jBRL_ADDRESS = '0xf2f77fe7b8e66571e0fca7104c4d670bf1c8d722';
export const ALICE_ADDRESS = '0xA11CE9D7B2D8f621dBDeaCA3D3863F1bb08De4B5';
export const BOB_ADDRESS = '0xB0b0905dC859b2A3471c94b62F8775b4D14C0a45';

export class CreditLineConfig {
  constructor(
    public readonly address: string,
    public readonly collateralRequirement: BigInt,
    public readonly priceIdentifier: string,
    // @ts-ignore
    public readonly version: i32
  ) {}
}

export const jEUR_USDC_CREDITLINE_V2 = new CreditLineConfig(
  jEUR_USDC_CREDITLINE_V2_ADDRESS,
  BigInt.fromString('1050000000000000000'),
  'EURUSD',
  2
);
export const jBRL_USDC_CREDITLINE_V2 = new CreditLineConfig(
  jBRL_USDC_CREDITLINE_V2_ADDRESS,
  BigInt.fromString('1250000000000000000'),
  'BRLUSD',
  2
);

export class TokenConfig {
  constructor(
    public readonly address: string,
    public readonly symbol: string,
    // @ts-ignore
    public readonly decimals: i32
  ) {}
}

export const USDC = new TokenConfig(USDC_ADDRESS, 'USDC', 6);
export const jEUR = new TokenConfig(jEUR_ADDRESS, 'jEUR', 18);
export const jBRL = new TokenConfig(jBRL_ADDRESS, 'jBRL', 18);
