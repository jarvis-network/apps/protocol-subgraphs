import { assert, clearStore, test, describe, afterEach } from 'matchstick-as/assembly/index';
import {
  createNewCreditLineEvent,
  createRemoveCreditLineEvent,
  handleNewCreditLines,
} from '../utils/events';
import { BigInt } from '@graphprotocol/graph-ts';

import {
  jEUR_USDC_CREDITLINE_V2,
  jBRL_USDC_CREDITLINE_V2,
  USDC,
  jEUR,
  USDC_ADDRESS,
  jEUR_ADDRESS,
  jBRL,
  jEUR_USDC_CREDITLINE_V2_ADDRESS,
  DEPLOYER_ADDRESS,
  jBRL_USDC_CREDITLINE_V2_ADDRESS,
  CreditLineConfig,
} from '../utils/constants';
import { handleNewCreditLine, handleRemoveCreditLine } from '../../src/mapping/deployer';

const DEPLOYER_ENTITY_TYPE = 'Deployer';
const CREDITLINE_ENTITY_TYPE = 'CreditLine';
const TOKEN_ENTITY_TYPE = 'Token';

describe('Deployer - CreditLine', () => {
  afterEach(() => {
    clearStore();
  });
  test('Should correclty initialize new creditLines v2 deployed', () => {
    const jEURCreditLineEvent = createNewCreditLineEvent(jEUR_USDC_CREDITLINE_V2, USDC, jEUR);
    const jBRLCreditLineEvent = createNewCreditLineEvent(jBRL_USDC_CREDITLINE_V2, USDC, jBRL);

    handleNewCreditLines([jEURCreditLineEvent, jBRLCreditLineEvent]);

    assert.entityCount(CREDITLINE_ENTITY_TYPE, 2);
    assert.fieldEquals(
      DEPLOYER_ENTITY_TYPE,
      DEPLOYER_ADDRESS.toLowerCase(),
      'creditLineCount',
      '2'
    );
    assert.fieldEquals(
      CREDITLINE_ENTITY_TYPE,
      jEUR_USDC_CREDITLINE_V2_ADDRESS.toLowerCase(),
      'version',
      '2'
    );
    assert.fieldEquals(
      CREDITLINE_ENTITY_TYPE,
      jBRL_USDC_CREDITLINE_V2_ADDRESS.toLowerCase(),
      'version',
      '2'
    );
  });
  test('Should not take into account creditLine that are not on version 2', () => {
    const jEUR_USDC_CREDITLINE_V1 = new CreditLineConfig(
      jEUR_USDC_CREDITLINE_V2_ADDRESS,
      BigInt.fromString('1050000000000000000'),
      'EURUSD',
      1
    );
    const jBRL_USDC_CREDITLINE_V3 = new CreditLineConfig(
      jBRL_USDC_CREDITLINE_V2_ADDRESS,
      BigInt.fromString('1250000000000000000'),
      'BRLUSD',
      3
    );
    const jEURCreditLineEvent = createNewCreditLineEvent(jEUR_USDC_CREDITLINE_V1, USDC, jEUR);
    const jBRLCreditLineEvent = createNewCreditLineEvent(jBRL_USDC_CREDITLINE_V3, USDC, jBRL);
    handleNewCreditLines([jEURCreditLineEvent, jBRLCreditLineEvent]);
    assert.entityCount(CREDITLINE_ENTITY_TYPE, 0);
  });
  test('Should correclty init tokens', () => {
    const jEURCreditLineEvent = createNewCreditLineEvent(jEUR_USDC_CREDITLINE_V2, USDC, jEUR);

    handleNewCreditLine(jEURCreditLineEvent);

    assert.fieldEquals(TOKEN_ENTITY_TYPE, USDC_ADDRESS.toLowerCase(), 'symbol', 'USDC');
    assert.fieldEquals(TOKEN_ENTITY_TYPE, jEUR_ADDRESS.toLowerCase(), 'symbol', 'jEUR');
  });

  test('Should correclty remove creditLine on SelfMintingDerivativeRemoved event', () => {
    const jEURCreditLineEvent = createNewCreditLineEvent(jEUR_USDC_CREDITLINE_V2, USDC, jEUR);
    handleNewCreditLine(jEURCreditLineEvent);
    assert.entityCount(CREDITLINE_ENTITY_TYPE, 1);
    assert.fieldEquals(
      DEPLOYER_ENTITY_TYPE,
      DEPLOYER_ADDRESS.toLowerCase(),
      'creditLineCount',
      '1'
    );
    const removeCreditLineEvent = createRemoveCreditLineEvent(jEUR_USDC_CREDITLINE_V2.address);
    handleRemoveCreditLine(removeCreditLineEvent);
    assert.entityCount(CREDITLINE_ENTITY_TYPE, 0);
    assert.fieldEquals(
      DEPLOYER_ENTITY_TYPE,
      DEPLOYER_ADDRESS.toLowerCase(),
      'creditLineCount',
      '0'
    );
    assert.fieldEquals(TOKEN_ENTITY_TYPE, USDC_ADDRESS.toLowerCase(), 'symbol', 'USDC');
    assert.fieldEquals(TOKEN_ENTITY_TYPE, jEUR_ADDRESS.toLowerCase(), 'symbol', 'jEUR');
  });
});
