import { Address, BigDecimal, BigInt, ethereum, log } from '@graphprotocol/graph-ts';
import {
  assert,
  clearStore,
  test,
  describe,
  afterEach,
  beforeEach,
  logStore,
} from 'matchstick-as/assembly/index';
import {
  createCreditLineDepositEvent,
  createCreditLineEndedSponsorPositionEvent,
  createCreditLineLiquidateEvent,
  createCreditLinePositionEvent,
  createCreditLineRedeemEvent,
  createCreditLineRepayEvent,
  createCreditLineWithdrawalEvent,
  createNewCreditLineEvent,
  handlePositionsCreated,
} from '../utils/events';
import { ETH_PRECISION, USD_PRECISION } from '../../src/utils/constants';
import {
  ALICE_ADDRESS,
  BOB_ADDRESS,
  USDC_ADDRESS,
  jEUR_ADDRESS,
  jEUR_USDC_CREDITLINE_V2,
  jEUR_USDC_CREDITLINE_V2_ADDRESS,
} from '../utils/constants';

import { handleNewCreditLine } from '../../src/mapping/deployer';
import {
  handleDeposit,
  handlePositionCreated,
  handleWithdrawal,
  handleRepay,
  handleRedeem,
  handleLiquidation,
  handleEndedSponsorPosition,
} from '../../src/mapping/creditLine';
import { getHistoryEntityId, getLPId } from '../../src/utils/id-generation';

import { USDC, jEUR } from '../utils/constants';
import { LPPosition, User } from '../../generated/schema';
import { calcRatio } from '../../src/helpers/math';
import { formatAmountTo18Decimals } from '../../src/utils/converters';

const CREDITLINE_ENTITY_TYPE = 'CreditLine';
const LP_POSITION_ENTITY_TYPE = 'LPPosition';
const DEPOSIT_ENTITY_TYPE = 'Deposit';
const WITHDRAW_ENTITY_TYPE = 'Withdraw';
const REDEEM_ENTITY_TYPE = 'Redeem';
const REPAY_ENTITY_TYPE = 'Repay';
const MINT_ENTITY_TYPE = 'Mint';
const LIQUIDATE_ENTITY_TYPE = 'Liquidate';
const USER_ENTITY_TYPE = 'User';

const aliceLpId = getLPId(
  Address.fromString(ALICE_ADDRESS),
  jEUR_USDC_CREDITLINE_V2_ADDRESS
).toLowerCase();
const aliceDepositedCollateral = BigInt.fromI32(15).times(
  BigInt.fromString(USD_PRECISION.toString())
);
const aliceTokenMinted = BigInt.fromI32(10).times(BigInt.fromString(ETH_PRECISION.toString()));
const aliceFeeAmount = BigInt.fromI32(1).times(BigInt.fromString(USD_PRECISION.toString()));

const bobLpId = getLPId(
  Address.fromString(BOB_ADDRESS),
  jEUR_USDC_CREDITLINE_V2_ADDRESS
).toLowerCase();
const bobDepositedCollateral = BigInt.fromI32(10).times(
  BigInt.fromString(USD_PRECISION.toString())
);
const bobTokenMinted = BigInt.fromI32(8).times(BigInt.fromString(ETH_PRECISION.toString()));
const bobFeeAmount = BigInt.fromI32(1).times(
  BigInt.fromString(USD_PRECISION.div(BigDecimal.fromString('10')).toString())
); // 0.1

describe('CreditLine - New Position', () => {
  beforeEach(() => {
    const jEURCreditLineEvent = createNewCreditLineEvent(jEUR_USDC_CREDITLINE_V2, USDC, jEUR);
    handleNewCreditLine(jEURCreditLineEvent);
  });
  afterEach(() => {
    clearStore();
  });
  test('Should correctly init a new Lp', () => {
    const alicePosition = createCreditLinePositionEvent(
      jEUR_USDC_CREDITLINE_V2_ADDRESS,
      ALICE_ADDRESS,
      aliceDepositedCollateral,
      aliceTokenMinted,
      aliceFeeAmount
    );
    handlePositionCreated(alicePosition);
    const expectedRatio = calcRatio(
      formatAmountTo18Decimals(aliceDepositedCollateral.minus(aliceFeeAmount), 6),
      aliceTokenMinted
    );
    //CreditLine entity
    assert.fieldEquals(
      CREDITLINE_ENTITY_TYPE,
      jEUR_USDC_CREDITLINE_V2_ADDRESS.toLowerCase(),
      'netCollateral',
      aliceDepositedCollateral.minus(aliceFeeAmount).toString()
    );
    assert.fieldEquals(
      CREDITLINE_ENTITY_TYPE,
      jEUR_USDC_CREDITLINE_V2_ADDRESS.toLowerCase(),
      'syntheticOutstanding',
      aliceTokenMinted.toString()
    );
    assert.fieldEquals(
      CREDITLINE_ENTITY_TYPE,
      jEUR_USDC_CREDITLINE_V2_ADDRESS.toLowerCase(),
      'ratio',
      expectedRatio.toString()
    );
    assert.fieldEquals(
      CREDITLINE_ENTITY_TYPE,
      jEUR_USDC_CREDITLINE_V2_ADDRESS.toLowerCase(),
      'feeAmount',
      aliceFeeAmount.toString()
    );

    //LP position entity
    assert.fieldEquals(
      LP_POSITION_ENTITY_TYPE,
      aliceLpId,
      'netCollateral',
      aliceDepositedCollateral.minus(aliceFeeAmount).toString()
    );
    assert.fieldEquals(
      LP_POSITION_ENTITY_TYPE,
      aliceLpId,
      'syntheticOutstanding',
      aliceTokenMinted.toString()
    );
    assert.fieldEquals(LP_POSITION_ENTITY_TYPE, aliceLpId, 'ratio', expectedRatio.toString());
    assert.fieldEquals(LP_POSITION_ENTITY_TYPE, aliceLpId, 'feeAmount', aliceFeeAmount.toString());

    const lpPosition = LPPosition.load(aliceLpId)!;
    assert.stringEquals(lpPosition.user, ALICE_ADDRESS.toLowerCase());

    const user = User.load(ALICE_ADDRESS.toLowerCase())!;
    const alicePositions = user.positions;
    assert.equals(ethereum.Value.fromI32(alicePositions.length), ethereum.Value.fromI32(1));
    assert.equals(
      ethereum.Value.fromString(alicePositions[0]),
      ethereum.Value.fromString(lpPosition.id)
    );
  });

  test('Should correctly handle multi Lps', () => {
    const alicePosition = createCreditLinePositionEvent(
      jEUR_USDC_CREDITLINE_V2_ADDRESS,
      ALICE_ADDRESS,
      aliceDepositedCollateral,
      aliceTokenMinted,
      aliceFeeAmount
    );
    const bobPosition = createCreditLinePositionEvent(
      jEUR_USDC_CREDITLINE_V2_ADDRESS,
      BOB_ADDRESS,
      bobDepositedCollateral,
      bobTokenMinted,
      bobFeeAmount
    );
    handlePositionsCreated([alicePosition, bobPosition]);

    const totalNetDepositedCollateral = aliceDepositedCollateral
      .minus(aliceFeeAmount)
      .plus(bobDepositedCollateral.minus(bobFeeAmount));
    const totalOutstandingSynthetics = aliceTokenMinted.plus(bobTokenMinted);
    const totalFees = aliceFeeAmount.plus(bobFeeAmount);
    assert.fieldEquals(
      CREDITLINE_ENTITY_TYPE,
      jEUR_USDC_CREDITLINE_V2_ADDRESS.toLowerCase(),
      'netCollateral',
      totalNetDepositedCollateral.toString()
    );
    assert.fieldEquals(
      CREDITLINE_ENTITY_TYPE,
      jEUR_USDC_CREDITLINE_V2_ADDRESS.toLowerCase(),
      'syntheticOutstanding',
      totalOutstandingSynthetics.toString()
    );
    assert.fieldEquals(
      CREDITLINE_ENTITY_TYPE,
      jEUR_USDC_CREDITLINE_V2_ADDRESS.toLowerCase(),
      'feeAmount',
      totalFees.toString()
    );

    assert.entityCount(USER_ENTITY_TYPE, 2);
    assert.entityCount(LP_POSITION_ENTITY_TYPE, 2);

    const aliceLpPosition = LPPosition.load(aliceLpId)!;
    assert.stringEquals(aliceLpPosition.user, ALICE_ADDRESS.toLowerCase());
    const bobLpPosition = LPPosition.load(bobLpId)!;
    assert.stringEquals(bobLpPosition.user, BOB_ADDRESS.toLowerCase());
  });
  test('Should correctly handle case where user borrow with additional collateral', () => {
    const alicePosition = createCreditLinePositionEvent(
      jEUR_USDC_CREDITLINE_V2_ADDRESS,
      ALICE_ADDRESS,
      aliceDepositedCollateral,
      aliceTokenMinted,
      aliceFeeAmount
    );
    handlePositionCreated(alicePosition);

    const newDepositedCollateral = BigInt.fromI32(15).times(
      BigInt.fromString(USD_PRECISION.toString())
    );

    const newTokenMinted = BigInt.fromI32(20).times(BigInt.fromString(ETH_PRECISION.toString()));
    const newFeeAmount = BigInt.fromI32(5).times(BigInt.fromString(USD_PRECISION.toString()));
    const expectedNetCollateral = aliceDepositedCollateral
      .minus(aliceFeeAmount)
      .plus(newDepositedCollateral)
      .minus(newFeeAmount);
    const expectedTokenOutstanding = aliceTokenMinted.plus(newTokenMinted);
    const expectedFeeAmount = aliceFeeAmount.plus(newFeeAmount);
    const expectedRatio = calcRatio(
      formatAmountTo18Decimals(expectedNetCollateral, 6),
      expectedTokenOutstanding
    );
    const additionalAlicePosition = createCreditLinePositionEvent(
      jEUR_USDC_CREDITLINE_V2_ADDRESS,
      ALICE_ADDRESS,
      newDepositedCollateral,
      newTokenMinted,
      newFeeAmount
    );
    additionalAlicePosition.block.number = BigInt.fromI32(2);
    handlePositionCreated(additionalAlicePosition);

    //CreditLine entity
    assert.fieldEquals(
      CREDITLINE_ENTITY_TYPE,
      jEUR_USDC_CREDITLINE_V2_ADDRESS.toLowerCase(),
      'netCollateral',
      expectedNetCollateral.toString()
    );
    assert.fieldEquals(
      CREDITLINE_ENTITY_TYPE,
      jEUR_USDC_CREDITLINE_V2_ADDRESS.toLowerCase(),
      'syntheticOutstanding',
      expectedTokenOutstanding.toString()
    );
    assert.fieldEquals(
      CREDITLINE_ENTITY_TYPE,
      jEUR_USDC_CREDITLINE_V2_ADDRESS.toLowerCase(),
      'ratio',
      expectedRatio.toString()
    );
    assert.fieldEquals(
      CREDITLINE_ENTITY_TYPE,
      jEUR_USDC_CREDITLINE_V2_ADDRESS.toLowerCase(),
      'feeAmount',
      expectedFeeAmount.toString()
    );

    //LP position entity
    assert.fieldEquals(
      LP_POSITION_ENTITY_TYPE,
      aliceLpId,
      'netCollateral',
      expectedNetCollateral.toString()
    );
    assert.fieldEquals(
      LP_POSITION_ENTITY_TYPE,
      aliceLpId,
      'syntheticOutstanding',
      expectedTokenOutstanding.toString()
    );
    assert.fieldEquals(LP_POSITION_ENTITY_TYPE, aliceLpId, 'ratio', expectedRatio.toString());
    assert.fieldEquals(
      LP_POSITION_ENTITY_TYPE,
      aliceLpId,
      'feeAmount',
      expectedFeeAmount.toString()
    );

    const user = User.load(ALICE_ADDRESS.toLowerCase())!;
    const aliceMintHistory = user.mintHistory;
    assert.equals(ethereum.Value.fromI32(aliceMintHistory.length), ethereum.Value.fromI32(2));
  });
  test('Should correctly add Mint transaction to the user', () => {
    const alicePosition = createCreditLinePositionEvent(
      jEUR_USDC_CREDITLINE_V2_ADDRESS,
      ALICE_ADDRESS,
      aliceDepositedCollateral,
      aliceTokenMinted,
      aliceFeeAmount
    );
    const eventId = getHistoryEntityId(alicePosition);
    handlePositionCreated(alicePosition);

    // Mint entity
    assert.fieldEquals(MINT_ENTITY_TYPE, eventId, 'user', ALICE_ADDRESS.toLowerCase());
    assert.fieldEquals(MINT_ENTITY_TYPE, eventId, 'action', 'Mint');
    assert.fieldEquals(MINT_ENTITY_TYPE, eventId, 'collateralToken', USDC_ADDRESS.toLowerCase());
    assert.fieldEquals(MINT_ENTITY_TYPE, eventId, 'feeAmount', aliceFeeAmount.toString());
    assert.fieldEquals(
      MINT_ENTITY_TYPE,
      eventId,
      'collateralDeposited',
      aliceDepositedCollateral.toString()
    );
    assert.fieldEquals(MINT_ENTITY_TYPE, eventId, 'syntheticMinted', aliceTokenMinted.toString());
  });
});

describe('CreditLine - EndedSponsorPosition', () => {
  beforeEach(() => {
    const jEURCreditLineEvent = createNewCreditLineEvent(jEUR_USDC_CREDITLINE_V2, USDC, jEUR);
    handleNewCreditLine(jEURCreditLineEvent);
    const aliceCreatePosition = createCreditLinePositionEvent(
      jEUR_USDC_CREDITLINE_V2_ADDRESS,
      ALICE_ADDRESS,
      aliceDepositedCollateral,
      aliceTokenMinted,
      aliceFeeAmount
    );
    handlePositionCreated(aliceCreatePosition);
  });
  afterEach(() => {
    clearStore();
  });
  test('Should correctly remove LP position when ended', () => {
    const aliceEndPosition = createCreditLineEndedSponsorPositionEvent(
      jEUR_USDC_CREDITLINE_V2_ADDRESS,
      ALICE_ADDRESS
    );
    handleEndedSponsorPosition(aliceEndPosition);

    //CreditLine entity
    assert.fieldEquals(
      CREDITLINE_ENTITY_TYPE,
      jEUR_USDC_CREDITLINE_V2_ADDRESS.toLowerCase(),
      'netCollateral',
      '0'
    );
    assert.fieldEquals(
      CREDITLINE_ENTITY_TYPE,
      jEUR_USDC_CREDITLINE_V2_ADDRESS.toLowerCase(),
      'syntheticOutstanding',
      '0'
    );
    assert.fieldEquals(
      CREDITLINE_ENTITY_TYPE,
      jEUR_USDC_CREDITLINE_V2_ADDRESS.toLowerCase(),
      'ratio',
      '0'
    );
    assert.fieldEquals(
      CREDITLINE_ENTITY_TYPE,
      jEUR_USDC_CREDITLINE_V2_ADDRESS.toLowerCase(),
      'feeAmount',
      aliceFeeAmount.toString()
    );

    //LP position entity
    assert.notInStore('LP_POSITION_ENTITY_TYPE', aliceLpId);
    assert.entityCount('LP_POSITION_ENTITY_TYPE', 0);
  });

  test('Should correctly add Redeem transaction to the user', () => {
    const lpPosition = LPPosition.load(aliceLpId)!;

    const aliceEndPosition = createCreditLineEndedSponsorPositionEvent(
      jEUR_USDC_CREDITLINE_V2_ADDRESS,
      ALICE_ADDRESS
    );
    handleEndedSponsorPosition(aliceEndPosition);
    const eventId = getHistoryEntityId(aliceEndPosition);

    // Redeem entity
    assert.fieldEquals(REDEEM_ENTITY_TYPE, eventId, 'user', ALICE_ADDRESS.toLowerCase());
    assert.fieldEquals(REDEEM_ENTITY_TYPE, eventId, 'action', 'Redeem');
    assert.fieldEquals(REDEEM_ENTITY_TYPE, eventId, 'collateralToken', USDC_ADDRESS.toLowerCase());
    assert.fieldEquals(
      REDEEM_ENTITY_TYPE,
      eventId,
      'collateralAmount',
      lpPosition.netCollateral.toString()
    );
    assert.fieldEquals(REDEEM_ENTITY_TYPE, eventId, 'syntheticToken', jEUR_ADDRESS.toLowerCase());
    assert.fieldEquals(
      REDEEM_ENTITY_TYPE,
      eventId,
      'tokenAmount',
      lpPosition.syntheticOutstanding.toString()
    );
    assert.fieldEquals(REDEEM_ENTITY_TYPE, eventId, 'isEndedPosition', true.toString());
  });
});
describe('CreditLine - Deposit', () => {
  beforeEach(() => {
    const jEURCreditLineEvent = createNewCreditLineEvent(jEUR_USDC_CREDITLINE_V2, USDC, jEUR);
    handleNewCreditLine(jEURCreditLineEvent);
    const aliceCreatePosition = createCreditLinePositionEvent(
      jEUR_USDC_CREDITLINE_V2_ADDRESS,
      ALICE_ADDRESS,
      aliceDepositedCollateral,
      aliceTokenMinted,
      aliceFeeAmount
    );
    handlePositionCreated(aliceCreatePosition);
  });
  afterEach(() => {
    clearStore();
  });
  test('Should correctly allow user to Deposit more collateral and inscrease his ratio', () => {
    const prevUserRatio = LPPosition.load(aliceLpId)!.ratio;

    const additionalDeposit = BigInt.fromI32(5).times(BigInt.fromString(USD_PRECISION.toString()));
    const expectedTotalAliceNetDeposit = aliceDepositedCollateral
      .minus(aliceFeeAmount)
      .plus(additionalDeposit);
    const expectedRatio = calcRatio(
      formatAmountTo18Decimals(expectedTotalAliceNetDeposit, 6),
      aliceTokenMinted
    );

    const aliceDeposit = createCreditLineDepositEvent(
      jEUR_USDC_CREDITLINE_V2_ADDRESS,
      ALICE_ADDRESS,
      additionalDeposit
    );
    handleDeposit(aliceDeposit);

    //CreditLine entity
    assert.fieldEquals(
      CREDITLINE_ENTITY_TYPE,
      jEUR_USDC_CREDITLINE_V2_ADDRESS.toLowerCase(),
      'netCollateral',
      expectedTotalAliceNetDeposit.toString()
    );
    assert.fieldEquals(
      CREDITLINE_ENTITY_TYPE,
      jEUR_USDC_CREDITLINE_V2_ADDRESS.toLowerCase(),
      'syntheticOutstanding',
      aliceTokenMinted.toString()
    );
    assert.fieldEquals(
      CREDITLINE_ENTITY_TYPE,
      jEUR_USDC_CREDITLINE_V2_ADDRESS.toLowerCase(),
      'ratio',
      expectedRatio.toString()
    );
    assert.fieldEquals(
      CREDITLINE_ENTITY_TYPE,
      jEUR_USDC_CREDITLINE_V2_ADDRESS.toLowerCase(),
      'feeAmount',
      aliceFeeAmount.toString()
    );

    //LP position entity
    assert.fieldEquals(
      LP_POSITION_ENTITY_TYPE,
      aliceLpId,
      'netCollateral',
      expectedTotalAliceNetDeposit.toString()
    );
    assert.fieldEquals(
      LP_POSITION_ENTITY_TYPE,
      aliceLpId,
      'syntheticOutstanding',
      aliceTokenMinted.toString()
    );
    assert.fieldEquals(LP_POSITION_ENTITY_TYPE, aliceLpId, 'ratio', expectedRatio.toString());
    assert.fieldEquals(LP_POSITION_ENTITY_TYPE, aliceLpId, 'feeAmount', aliceFeeAmount.toString());
    assert.assertTrue(prevUserRatio.le(expectedRatio));
  });

  test('Should correctly add Deposit transaction to the user', () => {
    const additionalDeposit = BigInt.fromI32(5).times(BigInt.fromString(USD_PRECISION.toString()));

    const aliceDeposit = createCreditLineDepositEvent(
      jEUR_USDC_CREDITLINE_V2_ADDRESS,
      ALICE_ADDRESS,
      additionalDeposit
    );
    aliceDeposit.transaction.from = Address.fromString(BOB_ADDRESS.toLowerCase());
    handleDeposit(aliceDeposit);
    const eventId = getHistoryEntityId(aliceDeposit);

    // Deposit entity
    assert.fieldEquals(DEPOSIT_ENTITY_TYPE, eventId, 'user', ALICE_ADDRESS.toLowerCase());
    assert.fieldEquals(DEPOSIT_ENTITY_TYPE, eventId, 'caller', BOB_ADDRESS.toLowerCase());
    assert.fieldEquals(DEPOSIT_ENTITY_TYPE, eventId, 'action', 'Deposit');
    assert.fieldEquals(DEPOSIT_ENTITY_TYPE, eventId, 'collateralToken', USDC_ADDRESS.toLowerCase());
    assert.fieldEquals(
      DEPOSIT_ENTITY_TYPE,
      eventId,
      'collateralAmount',
      additionalDeposit.toString()
    );
  });
});
describe('CreditLine - Withdraw', () => {
  beforeEach(() => {
    const jEURCreditLineEvent = createNewCreditLineEvent(jEUR_USDC_CREDITLINE_V2, USDC, jEUR);
    handleNewCreditLine(jEURCreditLineEvent);
    const aliceCreatePosition = createCreditLinePositionEvent(
      jEUR_USDC_CREDITLINE_V2_ADDRESS,
      ALICE_ADDRESS,
      aliceDepositedCollateral,
      aliceTokenMinted,
      aliceFeeAmount
    );
    handlePositionCreated(aliceCreatePosition);
  });
  afterEach(() => {
    clearStore();
  });
  test('Should correctly allow user to Withdraw collateral and decrease his ratio', () => {
    const prevUserRatio = LPPosition.load(aliceLpId)!.ratio;

    const collateralWithdrawn = BigInt.fromI32(5).times(
      BigInt.fromString(USD_PRECISION.toString())
    );
    const expectedTotalAliceNetDeposit = aliceDepositedCollateral
      .minus(aliceFeeAmount)
      .minus(collateralWithdrawn);
    const expectedRatio = calcRatio(
      formatAmountTo18Decimals(expectedTotalAliceNetDeposit, 6),
      aliceTokenMinted
    );

    const aliceWithdraw = createCreditLineWithdrawalEvent(
      jEUR_USDC_CREDITLINE_V2_ADDRESS,
      ALICE_ADDRESS,
      collateralWithdrawn
    );
    handleWithdrawal(aliceWithdraw);

    //CreditLine entity
    assert.fieldEquals(
      CREDITLINE_ENTITY_TYPE,
      jEUR_USDC_CREDITLINE_V2_ADDRESS.toLowerCase(),
      'netCollateral',
      expectedTotalAliceNetDeposit.toString()
    );
    assert.fieldEquals(
      CREDITLINE_ENTITY_TYPE,
      jEUR_USDC_CREDITLINE_V2_ADDRESS.toLowerCase(),
      'syntheticOutstanding',
      aliceTokenMinted.toString()
    );
    assert.fieldEquals(
      CREDITLINE_ENTITY_TYPE,
      jEUR_USDC_CREDITLINE_V2_ADDRESS.toLowerCase(),
      'ratio',
      expectedRatio.toString()
    );
    assert.fieldEquals(
      CREDITLINE_ENTITY_TYPE,
      jEUR_USDC_CREDITLINE_V2_ADDRESS.toLowerCase(),
      'feeAmount',
      aliceFeeAmount.toString()
    );

    //LP position entity
    assert.fieldEquals(
      LP_POSITION_ENTITY_TYPE,
      aliceLpId,
      'netCollateral',
      expectedTotalAliceNetDeposit.toString()
    );
    assert.fieldEquals(
      LP_POSITION_ENTITY_TYPE,
      aliceLpId,
      'syntheticOutstanding',
      aliceTokenMinted.toString()
    );
    assert.fieldEquals(LP_POSITION_ENTITY_TYPE, aliceLpId, 'ratio', expectedRatio.toString());
    assert.fieldEquals(LP_POSITION_ENTITY_TYPE, aliceLpId, 'feeAmount', aliceFeeAmount.toString());
    assert.assertTrue(prevUserRatio.gt(expectedRatio));
  });

  test('Should correctly add Withdraw transaction to the user', () => {
    const collateralWithdrawn = BigInt.fromI32(5).times(
      BigInt.fromString(USD_PRECISION.toString())
    );

    const aliceWithdraw = createCreditLineWithdrawalEvent(
      jEUR_USDC_CREDITLINE_V2_ADDRESS,
      ALICE_ADDRESS,
      collateralWithdrawn
    );
    handleWithdrawal(aliceWithdraw);

    const eventId = getHistoryEntityId(aliceWithdraw);

    // Withdraw entity
    assert.fieldEquals(WITHDRAW_ENTITY_TYPE, eventId, 'user', ALICE_ADDRESS.toLowerCase());
    assert.fieldEquals(WITHDRAW_ENTITY_TYPE, eventId, 'action', 'Withdraw');
    assert.fieldEquals(
      WITHDRAW_ENTITY_TYPE,
      eventId,
      'collateralToken',
      USDC_ADDRESS.toLowerCase()
    );
    assert.fieldEquals(
      WITHDRAW_ENTITY_TYPE,
      eventId,
      'collateralAmount',
      collateralWithdrawn.toString()
    );
  });
});
describe('CreditLine - Repay', () => {
  beforeEach(() => {
    const jEURCreditLineEvent = createNewCreditLineEvent(jEUR_USDC_CREDITLINE_V2, USDC, jEUR);
    handleNewCreditLine(jEURCreditLineEvent);
    const aliceCreatePosition = createCreditLinePositionEvent(
      jEUR_USDC_CREDITLINE_V2_ADDRESS,
      ALICE_ADDRESS,
      aliceDepositedCollateral,
      aliceTokenMinted,
      aliceFeeAmount
    );
    handlePositionCreated(aliceCreatePosition);
  });
  afterEach(() => {
    clearStore();
  });
  test('Should correctly allow user to Repay and increase his ratio', () => {
    const prevUserRatio = LPPosition.load(aliceLpId)!.ratio;
    const totalAliceNetDeposit = aliceDepositedCollateral.minus(aliceFeeAmount);
    const tokenRepaid = BigInt.fromI32(5).times(BigInt.fromString(ETH_PRECISION.toString()));
    const expectedTotalAliceOutstandingToken = aliceTokenMinted.minus(tokenRepaid);
    const expectedRatio = calcRatio(
      formatAmountTo18Decimals(aliceDepositedCollateral.minus(aliceFeeAmount), 6),
      expectedTotalAliceOutstandingToken
    );

    const aliceRepay = createCreditLineRepayEvent(
      jEUR_USDC_CREDITLINE_V2_ADDRESS,
      ALICE_ADDRESS,
      tokenRepaid,
      expectedTotalAliceOutstandingToken
    );
    handleRepay(aliceRepay);

    //CreditLine entity
    assert.fieldEquals(
      CREDITLINE_ENTITY_TYPE,
      jEUR_USDC_CREDITLINE_V2_ADDRESS.toLowerCase(),
      'netCollateral',
      totalAliceNetDeposit.toString()
    );
    assert.fieldEquals(
      CREDITLINE_ENTITY_TYPE,
      jEUR_USDC_CREDITLINE_V2_ADDRESS.toLowerCase(),
      'syntheticOutstanding',
      expectedTotalAliceOutstandingToken.toString()
    );
    assert.fieldEquals(
      CREDITLINE_ENTITY_TYPE,
      jEUR_USDC_CREDITLINE_V2_ADDRESS.toLowerCase(),
      'ratio',
      expectedRatio.toString()
    );
    assert.fieldEquals(
      CREDITLINE_ENTITY_TYPE,
      jEUR_USDC_CREDITLINE_V2_ADDRESS.toLowerCase(),
      'feeAmount',
      aliceFeeAmount.toString()
    );

    //LP position entity
    assert.fieldEquals(
      LP_POSITION_ENTITY_TYPE,
      aliceLpId,
      'netCollateral',
      totalAliceNetDeposit.toString()
    );
    assert.fieldEquals(
      LP_POSITION_ENTITY_TYPE,
      aliceLpId,
      'syntheticOutstanding',
      expectedTotalAliceOutstandingToken.toString()
    );

    assert.fieldEquals(LP_POSITION_ENTITY_TYPE, aliceLpId, 'ratio', expectedRatio.toString());
    assert.fieldEquals(LP_POSITION_ENTITY_TYPE, aliceLpId, 'feeAmount', aliceFeeAmount.toString());

    assert.assertTrue(prevUserRatio.lt(expectedRatio));
  });

  test('Should correctly add Repay transaction to the user', () => {
    const tokenRepaid = BigInt.fromI32(5).times(BigInt.fromString(ETH_PRECISION.toString()));
    const expectedTotalAliceOutstandingToken = aliceTokenMinted.minus(tokenRepaid);

    const aliceRepay = createCreditLineRepayEvent(
      jEUR_USDC_CREDITLINE_V2_ADDRESS,
      ALICE_ADDRESS,
      tokenRepaid,
      expectedTotalAliceOutstandingToken
    );
    handleRepay(aliceRepay);

    const eventId = getHistoryEntityId(aliceRepay);

    // Repay entity
    assert.fieldEquals(REPAY_ENTITY_TYPE, eventId, 'user', ALICE_ADDRESS.toLowerCase());
    assert.fieldEquals(REPAY_ENTITY_TYPE, eventId, 'action', 'Repay');
    assert.fieldEquals(REPAY_ENTITY_TYPE, eventId, 'syntheticToken', jEUR_ADDRESS.toLowerCase());
    assert.fieldEquals(REPAY_ENTITY_TYPE, eventId, 'tokenAmount', tokenRepaid.toString());
  });
});

describe('CreditLine - Redeem', () => {
  beforeEach(() => {
    const jEURCreditLineEvent = createNewCreditLineEvent(jEUR_USDC_CREDITLINE_V2, USDC, jEUR);
    handleNewCreditLine(jEURCreditLineEvent);
    const aliceCreatePosition = createCreditLinePositionEvent(
      jEUR_USDC_CREDITLINE_V2_ADDRESS,
      ALICE_ADDRESS,
      aliceDepositedCollateral,
      aliceTokenMinted,
      aliceFeeAmount
    );
    handlePositionCreated(aliceCreatePosition);
  });
  afterEach(() => {
    clearStore();
  });
  test('Should correctly allow user to Redeem and increase his ratio', () => {
    const prevUserRatio = LPPosition.load(aliceLpId)!.ratio;
    const totalAliceNetDeposit = aliceDepositedCollateral.minus(aliceFeeAmount);
    const collateralWithdrawn = BigInt.fromI32(1).times(
      BigInt.fromString(USD_PRECISION.toString())
    );
    const expectedNetCollateral = totalAliceNetDeposit.minus(collateralWithdrawn);
    const tokenRepaid = BigInt.fromI32(5).times(BigInt.fromString(ETH_PRECISION.toString()));
    const expectedTotalAliceOutstandingToken = aliceTokenMinted.minus(tokenRepaid);
    const expectedRatio = calcRatio(
      formatAmountTo18Decimals(expectedNetCollateral, 6),
      expectedTotalAliceOutstandingToken
    );

    const aliceRedeem = createCreditLineRedeemEvent(
      jEUR_USDC_CREDITLINE_V2_ADDRESS,
      ALICE_ADDRESS,
      collateralWithdrawn,
      tokenRepaid
    );
    handleRedeem(aliceRedeem);

    //CreditLine entity
    assert.fieldEquals(
      CREDITLINE_ENTITY_TYPE,
      jEUR_USDC_CREDITLINE_V2_ADDRESS.toLowerCase(),
      'netCollateral',
      expectedNetCollateral.toString()
    );
    assert.fieldEquals(
      CREDITLINE_ENTITY_TYPE,
      jEUR_USDC_CREDITLINE_V2_ADDRESS.toLowerCase(),
      'syntheticOutstanding',
      expectedTotalAliceOutstandingToken.toString()
    );
    assert.fieldEquals(
      CREDITLINE_ENTITY_TYPE,
      jEUR_USDC_CREDITLINE_V2_ADDRESS.toLowerCase(),
      'ratio',
      expectedRatio.toString()
    );
    assert.fieldEquals(
      CREDITLINE_ENTITY_TYPE,
      jEUR_USDC_CREDITLINE_V2_ADDRESS.toLowerCase(),
      'feeAmount',
      aliceFeeAmount.toString()
    );

    //LP position entity
    assert.fieldEquals(
      LP_POSITION_ENTITY_TYPE,
      aliceLpId,
      'netCollateral',
      expectedNetCollateral.toString()
    );
    assert.fieldEquals(
      LP_POSITION_ENTITY_TYPE,
      aliceLpId,
      'syntheticOutstanding',
      expectedTotalAliceOutstandingToken.toString()
    );

    assert.fieldEquals(LP_POSITION_ENTITY_TYPE, aliceLpId, 'ratio', expectedRatio.toString());
    assert.fieldEquals(LP_POSITION_ENTITY_TYPE, aliceLpId, 'feeAmount', aliceFeeAmount.toString());

    assert.assertTrue(prevUserRatio.lt(expectedRatio));
  });

  test('Should correctly allow user to Redeem and decrease his ratio', () => {
    const prevUserRatio = LPPosition.load(aliceLpId)!.ratio;
    const totalAliceNetDeposit = aliceDepositedCollateral.minus(aliceFeeAmount);
    const collateralWithdrawn = BigInt.fromI32(10).times(
      BigInt.fromString(USD_PRECISION.toString())
    );
    const expectedNetCollateral = totalAliceNetDeposit.minus(collateralWithdrawn);
    const tokenRepaid = BigInt.fromI32(5).times(BigInt.fromString(ETH_PRECISION.toString()));
    const expectedTotalAliceOutstandingToken = aliceTokenMinted.minus(tokenRepaid);
    const expectedRatio = calcRatio(
      formatAmountTo18Decimals(expectedNetCollateral, 6),
      expectedTotalAliceOutstandingToken
    );

    const aliceRedeem = createCreditLineRedeemEvent(
      jEUR_USDC_CREDITLINE_V2_ADDRESS,
      ALICE_ADDRESS,
      collateralWithdrawn,
      tokenRepaid
    );
    handleRedeem(aliceRedeem);

    //CreditLine entity
    assert.fieldEquals(
      CREDITLINE_ENTITY_TYPE,
      jEUR_USDC_CREDITLINE_V2_ADDRESS.toLowerCase(),
      'netCollateral',
      expectedNetCollateral.toString()
    );
    assert.fieldEquals(
      CREDITLINE_ENTITY_TYPE,
      jEUR_USDC_CREDITLINE_V2_ADDRESS.toLowerCase(),
      'syntheticOutstanding',
      expectedTotalAliceOutstandingToken.toString()
    );
    assert.fieldEquals(
      CREDITLINE_ENTITY_TYPE,
      jEUR_USDC_CREDITLINE_V2_ADDRESS.toLowerCase(),
      'ratio',
      expectedRatio.toString()
    );
    assert.fieldEquals(
      CREDITLINE_ENTITY_TYPE,
      jEUR_USDC_CREDITLINE_V2_ADDRESS.toLowerCase(),
      'feeAmount',
      aliceFeeAmount.toString()
    );

    //LP position entity
    assert.fieldEquals(
      LP_POSITION_ENTITY_TYPE,
      aliceLpId,
      'netCollateral',
      expectedNetCollateral.toString()
    );
    assert.fieldEquals(
      LP_POSITION_ENTITY_TYPE,
      aliceLpId,
      'syntheticOutstanding',
      expectedTotalAliceOutstandingToken.toString()
    );

    assert.fieldEquals(LP_POSITION_ENTITY_TYPE, aliceLpId, 'ratio', expectedRatio.toString());
    assert.fieldEquals(LP_POSITION_ENTITY_TYPE, aliceLpId, 'feeAmount', aliceFeeAmount.toString());
    assert.assertTrue(prevUserRatio.gt(expectedRatio));
  });

  test('Should correctly add Redeem transaction to the user', () => {
    const collateralWithdrawn = BigInt.fromI32(1).times(
      BigInt.fromString(USD_PRECISION.toString())
    );
    const tokenRepaid = BigInt.fromI32(5).times(BigInt.fromString(ETH_PRECISION.toString()));

    const aliceRedeem = createCreditLineRedeemEvent(
      jEUR_USDC_CREDITLINE_V2_ADDRESS,
      ALICE_ADDRESS,
      collateralWithdrawn,
      tokenRepaid
    );
    handleRedeem(aliceRedeem);

    const eventId = getHistoryEntityId(aliceRedeem);

    // Redeem entity
    assert.fieldEquals(REDEEM_ENTITY_TYPE, eventId, 'user', ALICE_ADDRESS.toLowerCase());
    assert.fieldEquals(REDEEM_ENTITY_TYPE, eventId, 'action', 'Redeem');
    assert.fieldEquals(REDEEM_ENTITY_TYPE, eventId, 'syntheticToken', jEUR_ADDRESS.toLowerCase());
    assert.fieldEquals(REDEEM_ENTITY_TYPE, eventId, 'tokenAmount', tokenRepaid.toString());
    assert.fieldEquals(REDEEM_ENTITY_TYPE, eventId, 'collateralToken', USDC_ADDRESS.toLowerCase());
    assert.fieldEquals(
      REDEEM_ENTITY_TYPE,
      eventId,
      'collateralAmount',
      collateralWithdrawn.toString()
    );
  });

  test('Should handle case where user position ended before redeem event', () => {
    const alicePosition = LPPosition.load(aliceLpId)!;
    const aliceEndPosition = createCreditLineEndedSponsorPositionEvent(
      jEUR_USDC_CREDITLINE_V2_ADDRESS,
      ALICE_ADDRESS
    );
    handleEndedSponsorPosition(aliceEndPosition);

    const aliceRedeem = createCreditLineRedeemEvent(
      jEUR_USDC_CREDITLINE_V2_ADDRESS,
      ALICE_ADDRESS,
      alicePosition.netCollateral,
      alicePosition.syntheticOutstanding
    );
    handleRedeem(aliceRedeem);

    //LP position entity
    assert.notInStore('LP_POSITION_ENTITY_TYPE', aliceLpId);
    assert.entityCount('LP_POSITION_ENTITY_TYPE', 0);
  });
});

describe('CreditLine - Liquidate', () => {
  beforeEach(() => {
    const jEURCreditLineEvent = createNewCreditLineEvent(jEUR_USDC_CREDITLINE_V2, USDC, jEUR);
    handleNewCreditLine(jEURCreditLineEvent);
    const aliceCreatePosition = createCreditLinePositionEvent(
      jEUR_USDC_CREDITLINE_V2_ADDRESS,
      ALICE_ADDRESS,
      aliceDepositedCollateral,
      aliceTokenMinted,
      aliceFeeAmount
    );
    handlePositionCreated(aliceCreatePosition);
  });
  afterEach(() => {
    clearStore();
  });
  test('Should correctly liquidate a part of user position and increase user ratio', () => {
    const prevUserRatio = LPPosition.load(aliceLpId)!.ratio;
    const aliceNetDeposit = aliceDepositedCollateral.minus(aliceFeeAmount);
    const collateralLiquidated = BigInt.fromI32(2).times(
      BigInt.fromString(USD_PRECISION.toString())
    );
    const collateralRewards = BigInt.fromI32(1).times(BigInt.fromString(USD_PRECISION.toString()));
    const expectedNetCollateral = aliceNetDeposit.minus(collateralLiquidated);
    const tokenAmountLiquidated = BigInt.fromI32(5).times(
      BigInt.fromString(ETH_PRECISION.toString())
    );
    const expectedTotalAliceOutstandingToken = aliceTokenMinted.minus(tokenAmountLiquidated);
    const expectedRatio = calcRatio(
      formatAmountTo18Decimals(expectedNetCollateral, 6),
      expectedTotalAliceOutstandingToken
    );

    const aliceLiquidation = createCreditLineLiquidateEvent(
      jEUR_USDC_CREDITLINE_V2_ADDRESS,
      ALICE_ADDRESS,
      BOB_ADDRESS,
      tokenAmountLiquidated,
      collateralLiquidated,
      collateralRewards,
      BigInt.fromI32(10)
    );
    handleLiquidation(aliceLiquidation);

    //CreditLine entity
    assert.fieldEquals(
      CREDITLINE_ENTITY_TYPE,
      jEUR_USDC_CREDITLINE_V2_ADDRESS.toLowerCase(),
      'netCollateral',
      expectedNetCollateral.toString()
    );
    assert.fieldEquals(
      CREDITLINE_ENTITY_TYPE,
      jEUR_USDC_CREDITLINE_V2_ADDRESS.toLowerCase(),
      'syntheticOutstanding',
      expectedTotalAliceOutstandingToken.toString()
    );
    assert.fieldEquals(
      CREDITLINE_ENTITY_TYPE,
      jEUR_USDC_CREDITLINE_V2_ADDRESS.toLowerCase(),
      'ratio',
      expectedRatio.toString()
    );
    assert.fieldEquals(
      CREDITLINE_ENTITY_TYPE,
      jEUR_USDC_CREDITLINE_V2_ADDRESS.toLowerCase(),
      'feeAmount',
      aliceFeeAmount.toString()
    );

    //LP position entity
    assert.fieldEquals(
      LP_POSITION_ENTITY_TYPE,
      aliceLpId,
      'netCollateral',
      expectedNetCollateral.toString()
    );
    assert.fieldEquals(
      LP_POSITION_ENTITY_TYPE,
      aliceLpId,
      'syntheticOutstanding',
      expectedTotalAliceOutstandingToken.toString()
    );

    assert.fieldEquals(LP_POSITION_ENTITY_TYPE, aliceLpId, 'ratio', expectedRatio.toString());
    assert.fieldEquals(LP_POSITION_ENTITY_TYPE, aliceLpId, 'feeAmount', aliceFeeAmount.toString());

    assert.assertTrue(prevUserRatio.lt(expectedRatio));
  });

  test('Should correctly fully liquidate user position and close his position', () => {
    const collateralRewards = BigInt.fromI32(1).times(BigInt.fromString(USD_PRECISION.toString()));
    const aliceNetDeposit = aliceDepositedCollateral.minus(aliceFeeAmount);

    const aliceLiquidation = createCreditLineLiquidateEvent(
      jEUR_USDC_CREDITLINE_V2_ADDRESS,
      ALICE_ADDRESS,
      BOB_ADDRESS,
      aliceTokenMinted,
      aliceNetDeposit,
      collateralRewards,
      BigInt.fromI32(10)
    );
    handleLiquidation(aliceLiquidation);

    //CreditLine entity
    assert.fieldEquals(
      CREDITLINE_ENTITY_TYPE,
      jEUR_USDC_CREDITLINE_V2_ADDRESS.toLowerCase(),
      'netCollateral',
      '0'
    );
    assert.fieldEquals(
      CREDITLINE_ENTITY_TYPE,
      jEUR_USDC_CREDITLINE_V2_ADDRESS.toLowerCase(),
      'syntheticOutstanding',
      '0'
    );
    assert.fieldEquals(
      CREDITLINE_ENTITY_TYPE,
      jEUR_USDC_CREDITLINE_V2_ADDRESS.toLowerCase(),
      'ratio',
      '0'
    );
    assert.fieldEquals(
      CREDITLINE_ENTITY_TYPE,
      jEUR_USDC_CREDITLINE_V2_ADDRESS.toLowerCase(),
      'feeAmount',
      aliceFeeAmount.toString()
    );

    //LP position entity
    assert.notInStore(LP_POSITION_ENTITY_TYPE, aliceLpId);
  });

  test('Should correctly add Liquidate transaction to the sponsor and liquidator for a not fully liquidation position', () => {
    const collateralLiquidated = BigInt.fromI32(2).times(
      BigInt.fromString(USD_PRECISION.toString())
    );
    const collateralRewards = BigInt.fromI32(1).times(BigInt.fromString(USD_PRECISION.toString()));

    const tokenAmountLiquidated = BigInt.fromI32(5).times(
      BigInt.fromString(ETH_PRECISION.toString())
    );

    const aliceLiquidation = createCreditLineLiquidateEvent(
      jEUR_USDC_CREDITLINE_V2_ADDRESS,
      ALICE_ADDRESS,
      BOB_ADDRESS,
      tokenAmountLiquidated,
      collateralLiquidated,
      collateralRewards,
      BigInt.fromI32(10)
    );
    handleLiquidation(aliceLiquidation);
    const eventId = getHistoryEntityId(aliceLiquidation);

    // Liquidate entity
    assert.fieldEquals(LIQUIDATE_ENTITY_TYPE, eventId, 'user', ALICE_ADDRESS.toLowerCase());
    assert.fieldEquals(LIQUIDATE_ENTITY_TYPE, eventId, 'liquidator', BOB_ADDRESS.toLowerCase());
    assert.fieldEquals(LIQUIDATE_ENTITY_TYPE, eventId, 'action', 'Liquidate');
    assert.fieldEquals(
      LIQUIDATE_ENTITY_TYPE,
      eventId,
      'syntheticToken',
      jEUR_ADDRESS.toLowerCase()
    );
    assert.fieldEquals(
      LIQUIDATE_ENTITY_TYPE,
      eventId,
      'tokenAmount',
      tokenAmountLiquidated.toString()
    );
    assert.fieldEquals(
      LIQUIDATE_ENTITY_TYPE,
      eventId,
      'collateralToken',
      USDC_ADDRESS.toLowerCase()
    );
    assert.fieldEquals(
      LIQUIDATE_ENTITY_TYPE,
      eventId,
      'collateralAmount',
      collateralLiquidated.toString()
    );
    assert.fieldEquals(
      LIQUIDATE_ENTITY_TYPE,
      eventId,
      'collateralRewardsAmount',
      collateralRewards.toString()
    );
    assert.fieldEquals(LIQUIDATE_ENTITY_TYPE, eventId, 'isEndedPosition', false.toString());

    // Alice user entity
    const aliceUser = User.load(ALICE_ADDRESS.toLowerCase())!;
    const aliceLiquidationHistory = aliceUser.liquidationHistory;
    assert.assertNull(aliceUser.get('liquidatorHistory'));
    assert.i32Equals(aliceLiquidationHistory.length, 1);
    assert.stringEquals(aliceLiquidationHistory[0], eventId);

    // Bob user entity
    const bobUser = User.load(BOB_ADDRESS.toLowerCase())!;
    const bobLiquidatorHistory = bobUser.liquidatorHistory;
    assert.assertNull(bobUser.get('liquidationHistory'));
    assert.i32Equals(bobLiquidatorHistory.length, 1);
    assert.stringEquals(bobLiquidatorHistory[0], eventId);
  });

  test('Should correctly add Liquidate transaction to the sponsor and liquidator for a fully liquidation position', () => {
    const collateralRewards = BigInt.fromI32(1).times(BigInt.fromString(USD_PRECISION.toString()));
    const aliceNetDeposit = aliceDepositedCollateral.minus(aliceFeeAmount);

    const aliceLiquidation = createCreditLineLiquidateEvent(
      jEUR_USDC_CREDITLINE_V2_ADDRESS,
      ALICE_ADDRESS,
      BOB_ADDRESS,
      aliceTokenMinted,
      aliceNetDeposit,
      collateralRewards,
      BigInt.fromI32(10)
    );
    handleLiquidation(aliceLiquidation);
    const eventId = getHistoryEntityId(aliceLiquidation);

    // Liquidate entity
    assert.fieldEquals(LIQUIDATE_ENTITY_TYPE, eventId, 'user', ALICE_ADDRESS.toLowerCase());
    assert.fieldEquals(LIQUIDATE_ENTITY_TYPE, eventId, 'liquidator', BOB_ADDRESS.toLowerCase());
    assert.fieldEquals(LIQUIDATE_ENTITY_TYPE, eventId, 'action', 'Liquidate');
    assert.fieldEquals(
      LIQUIDATE_ENTITY_TYPE,
      eventId,
      'syntheticToken',
      jEUR_ADDRESS.toLowerCase()
    );
    assert.fieldEquals(LIQUIDATE_ENTITY_TYPE, eventId, 'tokenAmount', aliceTokenMinted.toString());
    assert.fieldEquals(
      LIQUIDATE_ENTITY_TYPE,
      eventId,
      'collateralToken',
      USDC_ADDRESS.toLowerCase()
    );
    assert.fieldEquals(
      LIQUIDATE_ENTITY_TYPE,
      eventId,
      'collateralAmount',
      aliceNetDeposit.toString()
    );
    assert.fieldEquals(
      LIQUIDATE_ENTITY_TYPE,
      eventId,
      'collateralRewardsAmount',
      collateralRewards.toString()
    );
    assert.fieldEquals(LIQUIDATE_ENTITY_TYPE, eventId, 'isEndedPosition', true.toString());

    // Alice user entity
    const aliceUser = User.load(ALICE_ADDRESS.toLowerCase())!;
    const aliceLiquidationHistory = aliceUser.liquidationHistory;
    assert.assertNull(aliceUser.get('liquidatorHistory'));
    assert.i32Equals(aliceLiquidationHistory.length, 1);
    assert.stringEquals(aliceLiquidationHistory[0], eventId);

    // Bob user entity
    const bobUser = User.load(BOB_ADDRESS.toLowerCase())!;
    const bobLiquidatorHistory = bobUser.liquidatorHistory;
    assert.assertNull(bobUser.get('liquidationHistory'));
    assert.i32Equals(bobLiquidatorHistory.length, 1);
    assert.stringEquals(bobLiquidatorHistory[0], eventId);
  });
});
